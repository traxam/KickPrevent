/*
 * Copyright (C) 2016 tr808axm - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by tr808axm <tr808axm@gmail.com>
 */

package de.tr808axm.bungeecord.kickprevent;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * KickPrevent-plugin's main class.
 * Created by tr808axm on 14.11.2016.
 */
public class KickPreventMain extends Plugin implements Listener {
    private File configFile;
    private ServerInfo defaultServer;
    @Override
    public void onLoad() {
        super.onLoad();
        getLogger().info("Loaded!");
    }

    @Override
    public void onEnable() {
        super.onEnable();
        configFile = new File(getDataFolder(), "config.yml");
        try {
            if (!configFile.exists()) {
                getLogger().info("Configuration file not found, creating...");
                configFile.getParentFile().mkdirs();
                InputStream defaultConfig = getResourceAsStream("config.yml");
                Files.copy(defaultConfig, configFile.toPath());
            }
            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            String defaultServerString = configuration.getString("default-server");
            defaultServer = getProxy().getServerInfo(defaultServerString);
            if (defaultServer == null) {
                getLogger().severe("FATAL: Could not get default server: " + defaultServerString + " could not be found!");
                return;
            }
        } catch (IOException e) {
            getLogger().severe("FATAL: Could not load configuration: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            return;
        }
        getProxy().getPluginManager().registerListener(this, this);
        getLogger().info("Enabled!");
    }

    @Override
    public void onDisable() {
        super.onDisable();
        getLogger().info("Disabled!");
    }

    @EventHandler
    public void onPlayerKick(ServerKickEvent e) {
        if (defaultServer.equals(e.getKickedFrom())) return; // Do not prohibit kicks from default server.
        e.setCancelled(true);
        e.setCancelServer(defaultServer);
        e.getPlayer().sendMessage(e.getKickReasonComponent());
    }
}
